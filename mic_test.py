#!/usr/bin/env python3
#-------------------------------------------------------------------------------
# Python script to record one multichannel wav file with microphone array
# created 2023-08-03 - 2023-10.26, version 1.1
#-------------------------------------------------------------------------------

import sounddevice as sd

from scipy.io.wavfile import write

T = 8 # record duration [s]
fs = 48000 # sampling frequency [Hz]

print("recording for {} s".format(T))
recording = sd.rec(int(T * fs), samplerate = fs, channels = 8, blocking = True)

# change the channel order, to reflect the physical position
# of the microphone
# Order of microphones from channel 0 to 7:
# L1, L2, L3, L4, P1, P2, P3, P4
# Desired order of microphones (around the circular array clockwise):
# P1, L1, P2, L2, P3, L3, P4, L4
# samples are in rows, channels are in columns
recording = recording[:, (4, 0, 5, 1, 6, 2, 7, 3)]

# Save as a WAV file (with step index in name)
wav_name = "mic_test_ordered.wav"
print("saving recording as {}".format(wav_name))
write(wav_name, fs, recording)

