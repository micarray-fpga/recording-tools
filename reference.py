#!/usr/bin/env python3
#-------------------------------------------------------------------------------
# Record sine sweeps with reference microphone
# created 2023-07-27, version 1.0
#-------------------------------------------------------------------------------

import time
import sounddevice as sd

# for signal processing
import numpy as np
from scipy.signal import chirp
from scipy.io.wavfile import write

#-------------------------------------------------------------------------------
# create logarithmic sine sweep from 20 Hz to 20kHz
# @returns the array of samples
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.chirp.html
T = 1 # sweep duration [s]
fs = 48000 # sampling frequency [Hz]
def sinesweep():
	# time vector, T*fs + 1 points (including upper limit)
	t = np.linspace(0, T, num = T*fs + 1)
	w = chirp(t, f0 = 20, f1 = 20000, t1 = T, method = 'logarithmic', phi = 90)
	silence = np.zeros(int(fs * 0.1)) # 100 ms silence
	# repeat 5 times
	w = np.concatenate((w, w, w, w, w, silence), axis = None)
	return w

#-------------------------------------------------------------------------------
# create sine sweep signal
sweep = sinesweep()

time.sleep(60)

recording = sd.playrec(sweep, samplerate = fs, channels = 1, blocking = True)

write('reference_recording.wav', fs, recording)


