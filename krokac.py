#!/usr/bin/env python3
#-------------------------------------------------------------------------------
# Python script to control stepper motor, play sine sweeps with speaker
# and record them with microphone array
# The stepper is controlled by Arduino UNO with firmata firmware
# run as: `python3 krokac.py`
# created 2023-06-29 - 2023-10-26, version 1.4.2
#-------------------------------------------------------------------------------

import time
import datetime
import os

# Arduino control
# pip3 install pyfirmata
from pyfirmata import Arduino, util

# sound recording
# pip3 install sounddevice
import sounddevice as sd

# for signal processing
import numpy as np
from scipy.signal import chirp
from scipy.io.wavfile import write

#-------------------------------------------------------------------------------

# create logarithmic sine sweep from 20 Hz to 20kHz
# @returns the array of samples
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.chirp.html
T = 1 # sweep duration [s]
fs = 48000 # sampling frequency [Hz]
def sinesweep():
	# time vector, T*fs + 1 points (including upper limit)
	t = np.linspace(0, T, num = int(T*fs) + 1)
	# start in value 0 -> phase 90
	sw = chirp(t, f0 = 20, f1 = 20000, t1 = T, method = 'logarithmic', phi = 90)
	silence = np.zeros(int(fs * 0.1)) # 100 ms silence
	sw = np.concatenate((silence, sw, silence, silence), axis=None)
	return sw

#-------------------------------------------------------------------------------
# try connect arduino
try:
	# serial port name for linux
	board = Arduino('/dev/ttyACM0')
except Exception as err:
	print(err)
	exit(1)

print("setting pins")
# arduino pins
DIR = board.get_pin("D:2:o");
STP = board.get_pin("D:3:o");
SLP = board.get_pin("D:4:o");
M2 = board.get_pin("D:7:o");
M1 = board.get_pin("D:6:o");
M0 = board.get_pin("D:5:o");

SLP.write(0) # driver sleep

# initial step direction
DIR.write(1)

# full step mode
M2.write(0)
M1.write(0)
M0.write(0)

SLP.write(1) # driver not sleep

#-------------------------------------------------------------------------------
# do one step with the motor
def krok():
	STP.write(0)
	time.sleep(.05)
	STP.write(1)
	time.sleep(.1)
	STP.write(0)

#-------------------------------------------------------------------------------

# create sine sweep signal
sweep = sinesweep()

# get date
date = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")
print(date)

# create output folder
folder = "measurement_" + date
if not os.path.exists(folder):
	os.makedirs(folder)

print("output folder: " + folder)

input("check the correct microphone position\npress enter to continue")

print("starting in 60 seconds, get ready...")
time.sleep(60)
print("GO")

# Measuring loop
# The motor does 200 steps per full rotation
for a in range(0, 200):
	angle = 360/200 * a
	print("step {}, angle {}˚".format(a, angle))
	# measure every 10 steps (18˚)
	#if a % 10 == 0:
	# measure every 2 steps (3.6˚)
	if a % 2 == 0:
		time.sleep(1) # wait for motor to settle
		#SLP.write(0) # driver sleep for silence
		#--------------------
		# play the sound and record
		# https://realpython.com/playing-and-recording-sound-python/
		# increase latency to prevent buffer underruns in ALSA
		recording = sd.playrec(sweep, samplerate = fs, channels = 8,
			blocking = True, latency = 0.12)
		time.sleep(.5)
		#SLP.write(1) # driver not sleep
		# cut the silence at the beginning (200ms)
		recording = recording[round(fs * 0.2):, :]
		#--------------------
		# change the channel order, to reflect the physical position
		# of the microphone
		# Order of microphones from channel 0 to 7:
		# L1, L2, L3, L4, P1, P2, P3, P4
		# Desired order of microphones (around the circular array clockwise):
		# P1, L1, P2, L2, P3, L3, P4, L4
		# samples are in rows, channels are in columns
		recording = recording[:, (4, 0, 5, 1, 6, 2, 7, 3)]
		# Save as a WAV file (with step index in name)
		write(folder + '/' + 'krok_{:03d}.wav'.format(int(a)), fs, recording)
	krok()

SLP.write(0) # driver sleep

exit(0)


